
/**
 * This code was generated by "react-native codegen-lib-harmony"
 */

#pragma once

#include "RNOH/Package.h"
#include "RNOH/ArkTSTurboModule.h"
#include "RNOH/generated/turbo_modules/BackgroundTimerTurboModule.h"

namespace rnoh {

class BaseReactNativeBackgroundTimerPackageTurboModuleFactoryDelegate : public TurboModuleFactoryDelegate {
  public:
    SharedTurboModule createTurboModule(Context ctx, const std::string &name) const override
    {
        if (name == "BackgroundTimerTurboModule") {
            return std::make_shared<BackgroundTimerTurboModule>(ctx, name);
        }
        return nullptr;
    };
};

class BaseReactNativeBackgroundTimerPackageEventEmitRequestHandler : public EventEmitRequestHandler {
  public:
    void handleEvent(Context const &ctx) override
    {
        auto eventEmitter = ctx.shadowViewRegistry->getEventEmitter<facebook::react::EventEmitter>(ctx.tag);
        if (eventEmitter == nullptr) {
            return;
        }

        std::vector<std::string> supportedEventNames = {
            // 添加支持的事件名称
        };
        if (std::find(supportedEventNames.begin(),
                      supportedEventNames.end(),
                      ctx.eventName) != supportedEventNames.end()) {
            eventEmitter->dispatchEvent(ctx.eventName, ArkJS(ctx.env).getDynamic(ctx.payload));
        }
    }
};


class BaseReactNativeBackgroundTimerPackage : public Package {
  public:
    BaseReactNativeBackgroundTimerPackage(Package::Context ctx) : Package(ctx){};

    std::unique_ptr<TurboModuleFactoryDelegate> createTurboModuleFactoryDelegate() override
    {
        return std::make_unique<BaseReactNativeBackgroundTimerPackageTurboModuleFactoryDelegate>();
    }

    std::vector<facebook::react::ComponentDescriptorProvider> createComponentDescriptorProviders() override
    {
        return {
        };
    }

    ComponentJSIBinderByString createComponentJSIBinderByName() override
    {
        return {
        };
    };

    EventEmitRequestHandlers createEventEmitRequestHandlers() override
    {
        return {
            std::make_shared<BaseReactNativeBackgroundTimerPackageEventEmitRequestHandler>(),
        };
    }
};

} // namespace rnoh
