/*
*
* Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
* Use of this source code is governed by a MIT license that can be
* found in the LICENSE file
*
*/

import { RNPackage, WorkerTurboModuleFactory } from '@rnoh/react-native-openharmony/ts';
import type { WorkerTurboModule, WorkerTurboModuleContext } from '@rnoh/react-native-openharmony/ts';
import { BackgroundTimerTurboModule } from './BackgroundTimerTurboModule';

class BackgroundTimerTurboModuleFactory extends WorkerTurboModuleFactory {
  createTurboModule(name: string): WorkerTurboModule | null {
    if (name === 'BackgroundTimerTurboModule') {
      return new BackgroundTimerTurboModule(this.ctx);
    }
    return null;
  }

  hasTurboModule(name: string): boolean {
    return name === 'BackgroundTimerTurboModule';
  }
}

export class BackgroundTimerTurboModulePackage extends RNPackage {
  createWorkerTurboModuleFactory(ctx: WorkerTurboModuleContext): WorkerTurboModuleFactory {
    return new BackgroundTimerTurboModuleFactory(ctx);
  }
}
